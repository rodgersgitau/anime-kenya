import {
    GraphQLObjectType
} from 'graphql';

import {
    addMember
} from './member';


module.exports = {
    addMember
}