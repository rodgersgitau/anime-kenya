import {
    GraphQLID,
    GraphQLString,
    GraphQLInt,
    GraphQLNonNull
} from 'graphql';

import Member from '../../models/member.js';
import MemberType from '../types/member';


const addMember = {
    description: "Create a new member",
    type: MemberType,
    args: {
        id: {
            type: GraphQLID
        },
        name: {
            type: new GraphQLNonNull(GraphQLString)
        },
        joinDate: {
            type: GraphQLString
        },
        rank: {
            type: GraphQLInt
        },
    },
    resolve(parent, args) {
        let member = new Member({
            name: args.name,
            joinDate: args.joinDate,
            rank: args.rank,
        });

        return member.save();
    }
}


module.exports = {
    addMember
}