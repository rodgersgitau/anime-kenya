import {
    GraphQLSchema,
    GraphQLObjectType
} from 'graphql';

import RootQuery from './queries';
import Mutations from './mutations';

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: Mutations
    })
})