import {
    GraphQLList
} from 'graphql';

import Member from '../../models/member.js';
import MemberType from '../types/member';

const member = {
    description: "Returns a single member",
    type: MemberType,
    resolve(parent, args) {
        return Member.findById(args.id);
    }
}

const allMembers = {
    description: "A list of all members",
    type: GraphQLList(MemberType),
    resolve(parent, args) {
        return Member.find({});
    }
}


module.exports = {
    member,
    allMembers
}