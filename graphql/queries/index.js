import {
    GraphQLObjectType,
} from 'graphql';

import {
    member,
    allMembers
} from './member';

import {
    author,
    allAuthors
} from './author';

import {
    anime,
    allAnimes
} from './anime';

const RootQuery = new GraphQLObjectType({
    name: "RootQueryType",
    fields: {
        member,
        allMembers,
        author,
        allAuthors,
        anime,
        allAnimes
    }

});

module.exports = RootQuery