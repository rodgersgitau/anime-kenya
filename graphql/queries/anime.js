import {
    GraphQLList
} from 'graphql';

import Anime from '../../models/anime.js';
import AnimeType from '../types/anime';

const anime = {
    description: "Returns a single anime",
    type: AnimeType,
    resolve(parent, args) {
        return Anime.findById(args.id);
    }
}

const allAnimes = {
    description: "A list of all animes",
    type: GraphQLList(AnimeType),
    resolve(parent, args) {
        return Anime.find({});
    }
}


module.exports = {
    anime,
    allAnimes
}