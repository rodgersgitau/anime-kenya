import {
    GraphQLList
} from 'graphql';

import Author from '../../models/author.js';
import AuthorType from '../types/author';

const author = {
    description: "Returns a single author",
    type: AuthorType,
    resolve(parent, args) {
        return Author.findById(args.id);
    }
}

const allAuthors = {
    description: "A list of all authors",
    type: GraphQLList(AuthorType),
    resolve(parent, args) {
        return Author.find({});
    }
}


module.exports = {
    author,
    allAuthors
}