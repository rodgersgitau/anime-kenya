import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString
} from 'graphql';

const AnimeType = new GraphQLObjectType({
    name: "Anime",
    description: "A member of the Anime Kenya Community",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        },
        synopsis: {
            type: GraphQLString
        },
        authorId: {
            type: GraphQLString
        }
    })
})

module.exports = AnimeType;