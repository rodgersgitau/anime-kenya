 import {
     GraphQLObjectType,
     GraphQLID,
     GraphQLString,
     GraphQLInt,
 } from 'graphql';

 const MemberType = new GraphQLObjectType({
     name: "Member",
     description: "A member of the Anime Kenya Community",
     fields: () => ({
         id: {
             type: GraphQLID
         },
         name: {
             type: GraphQLString
         },
         joinDate: {
             type: GraphQLString
         },
         rank: {
             type: GraphQLInt
         }
     })
 })

 module.exports = MemberType;