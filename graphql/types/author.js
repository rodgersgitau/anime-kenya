import {
    GraphQLObjectType,
    GraphQLID,
    GraphQLString,
} from 'graphql';

const AuthorType = new GraphQLObjectType({
    name: "Author",
    description: "A member of the Anime Kenya Community",
    fields: () => ({
        id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        }
    })
})

module.exports = AuthorType;