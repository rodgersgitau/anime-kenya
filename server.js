import express from 'express';
import graphqlHTTP from 'express-graphql';
import cors from 'cors';
import mongoose from './config';
import schema from './graphql';


const app = express();
const db = mongoose();
const port = process.env.PORT || 9027;

app.use('*', cors());

app.use(
    "/graphql",
    graphqlHTTP({
        schema,
        graphiql: true
    })
);


// Server Up and Running 
app.listen(port, () => {
    console.log(`=> Server Running on Port ${port}`);
});