import mongoose from 'mongoose';
const Schema = mongoose.Schema


const animeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    synopsis: {
        type: String,
        required: true
    },
    authorId: {
        type: String,
        required: true
    }
})


module.exports = mongoose.model("Anime", animeSchema);