import mongoose from 'mongoose';
const Schema = mongoose.Schema


const memberSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    joinDate: {
        type: String,
    },
    rank: {
        type: Number,
        default: 5
    }
})

module.exports = mongoose.model("Member", memberSchema);