import mongoose from 'mongoose';
import dotenv from 'dotenv';


const config = dotenv.config();

if (config.error) {
    throw config.error
}

const {
    DB_USER,
    DB_PASS
} = process.env;


module.exports = () => {
    mongoose.Promise = global.Promise;
    const db = mongoose.connect(
            `mongodb://${DB_USER}:${DB_PASS}@ds239911.mlab.com:39911/anime-kenya`, {
                useNewUrlParser: true
            }
        ).then(() => {
            console.log('Database connection successful')
        })
        .catch(err => {
            console.error('Database connection error')
        })
    return db;
}